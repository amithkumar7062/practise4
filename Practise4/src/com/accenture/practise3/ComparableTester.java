package com.accenture.practise3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ComparableTester {
	public static void main(String[] args) {
		ComparableDemo cd1 = new ComparableDemo(104, "Java", 10, 20000);
		ComparableDemo cd2 = new ComparableDemo(101, "C++", 20, 5000);
		ComparableDemo cd3 = new ComparableDemo(102, "C", 8, 8000);
		ComparableDemo cd4 = new ComparableDemo(103, "Python", 6, 3000);
		List<ComparableDemo> list = new ArrayList<>(); 
		list.add(cd1);
		list.add(cd2);
		list.add(cd3);
		list.add(cd4);
		Collections.sort(list);
		ComparableService comser = new ComparableService();
		comser.printAllCourseFeesSorted(list);
	}

}
