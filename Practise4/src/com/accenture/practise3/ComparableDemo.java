package com.accenture.practise3;

public class ComparableDemo implements Comparable<ComparableDemo>{
	private int courseId;
	private String courseName;
	private int courseDuration;
	private int courseFees;
	public ComparableDemo(int courseId, String courseName, int courseDuration, int courseFees) {
		super();
		this.courseId = courseId;
		this.courseName = courseName;
		this.courseDuration = courseDuration;
		this.courseFees = courseFees;
	}
	public int getCourseId() {
		return courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public int getCourseDuration() {
		return courseDuration;
	}
	public int getCourseFees() {
		return courseFees;
	}
	@Override
	public int compareTo(ComparableDemo com) {
		if(this.courseFees > com.getCourseFees())
			return 1;
		else if(this.courseFees < com.getCourseFees())
			return -1;
		else
		return 0;
	}
	

}
