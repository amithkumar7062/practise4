package com.accenture.practise3;

import java.util.List;

public class ComparableService {

	public ComparableService() {
		super();
	}

	public void printAllCourseFeesSorted(List<ComparableDemo> comde) {
		comde.forEach(comparableservice -> {
			System.out.println("Course Id:" + comparableservice.getCourseId());
			System.out.println("Course Name:" + comparableservice.getCourseName());
			System.out.println("Course Duration:" + comparableservice.getCourseDuration());
			System.out.println("Course Fees:" + comparableservice.getCourseFees());
		});
	}

}
